
var csvFile = {
    size:0,
    dataFile:[]
};

let table=document.getElementById("liste");

function sortingByID(){
  document.getElementById("liste").innerHTML = ""
  sortingbySeriesID(csvFile.dataFile);
  generateTable(table,csvFile.dataFile);  
}

function sortingByDate(){
  document.getElementById("liste").innerHTML = ""
  sortingbyDate(csvFile.dataFile);
  generateTable(table,csvFile.dataFile);  
  
}

function seriesTotal(){
  seriesArray(csvFile.dataFile); 
}


function readFile(input) {
 	  let file = input.files[0];
		let reader = new FileReader();
        reader.readAsText(file);
		reader.onload = function (e) {
 			console.log(e);
			csvFile.size = e.total;
			csvFile.dataFile = e.target.result
          console.log(csvFile.dataFile)
          parseData(csvFile.dataFile)
          generateTable(table,csvFile.dataFile)  
	}
}

function parseData(data){
    let csvArray = [];
    let rows = data.split("\n").slice(1);
    rows.forEach(row => {
        csvArray.push(row.split(","));
    });
    csvFile.dataFile=csvArray; 
}

function seriesArray(data) {
var diffSeries = []
for (element in data){
    if (diffSeries.includes(data[element][0])==false){
      diffSeries.push(data[element][0]);
  }
}
document.getElementById("demo").innerHTML=diffSeries;
}

function generateTable(table, data) {
  for (let element of data) {
    let row = table.insertRow();
    for (key in element) {
      let cell = row.insertCell();
      let text = document.createTextNode(element[key]);
      cell.appendChild(text);
    }
  }
}


function sortingbySeriesID(data){
  data.sort(compareBySeriesID);  
}
function sortingbyDate(data){
  data.sort(compareByDate);  
}


 var comparatorComposer = function(compareBy, nextComparator) { 
  var comparator = function(a, b) { 
      return ((a[compareBy] < b[compareBy]) ? -1 : ((a[compareBy] > b[compareBy]) ? 1 : (nextComparator ? nextComparator(a, b) : 0))); 
  }; 
  return comparator; 
}; 

var compareBySeriesID = comparatorComposer(0); 
var compareByDate = comparatorComposer(1); 